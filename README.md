# MBTESTE

Projeto para teste em Mercado  Bitcoin

##  Como usar

Para fazer o build da imagem docker utilizamos o seguinte comando:

```shell
docker-compose build
```

Para executar subir o serviços utilizamos:
```shell
docker-compose up
```

Para subir as tabelas e carga inicial de dados:
```shell
docker-compose exec web flask db upgrade
```

Rodar teste, Obs falta terminar teste
```shell
docker-compose exec web  pytest --cov=mb_teste ./test/ -s

---------- coverage: platform linux, python 3.7.11-final-0 -----------
Name                           Stmts   Miss  Cover
--------------------------------------------------
__init__.py                        5      0   100%
api/__init__.py                   10      1    90%
api/representations.py            26     11    58%
api/teste/__init__.py              0      0   100%
api/teste/schema.py                5      0   100%
api/teste/service.py              31      2    94%
api/teste/viewer.py               29      0   100%
app.py                             4      1    75%
app_factory.py                    51      1    98%
calculadora/MMS.py                10      5    50%
config/__init__.py                23      6    74%
database/__init__.py              47     28    40%
database/constante.py              1      0   100%
database/enum.py                  23      0   100%
database/models.py                40      8    80%
extensions.py                     18      5    72%
handlers.py                       12      1    92%
script/atualizacao_diaria.py      28     18    36%
script/valida_dados.py            14      7    50%
utils.py                          14      8    43%
--------------------------------------------------
TOTAL                            391    102    74%                           424    135    68%
```

##  link API local
para acessa o swagger (documentacao das rotas)

http://localhost:5015/docs/


## job 

As jobs inicia junto com o servico e roda uma vez por dia:

```python
    def configure_schedulers():
        # OBS: api do MB nao e publica nao consegui fazer request fora da web
        from mb_teste.script.atualizacao_diaria import att_diaria
        from mb_teste.script.valida_dados import validar_ultimos_360_dias
        sched = BackgroundScheduler(daemon=True)
        sched.add_job(att_diaria, 'interval', days=1)
        sched.add_job(validar_ultimos_360_dias, 'interval', days=1)
        sched.start()
```

* att_diaria (Rotina que busca diariamente os dados na api MB calcula a MMS e salva na tabela)
* validar_ultimos_360_dias (valida se todos os ultimos 360 dias estao na tabela calculada MMS)