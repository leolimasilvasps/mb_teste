FROM python:3.7-stretch
LABEL maintainer=""

LABEL fileversion=v1.00

WORKDIR /app/mb_teste

ENV PYTHONUNBUFFERED=0
ENV TZ=America/Sao_Paulo
ENV FLASK_APP=/app/mb_teste/mb_teste/app.py



RUN apt-get update -y && apt install -y uwsgi uwsgi-src uuid-dev libcap-dev default-jre

RUN export PYTHON=python3.7 && \ 
    uwsgi --build-plugin "/usr/src/uwsgi/plugins/python python37" && \
    mv python37_plugin.so /usr/lib/uwsgi/plugins/python37_plugin.so && \
    chmod 644 /usr/lib/uwsgi/plugins/python37_plugin.so


COPY requirements.txt .

RUN pip install -Ur requirements.txt --quiet

COPY . /app/mb_teste

RUN python setup.py develop

EXPOSE 5000

ENTRYPOINT ["/bin/bash", "/app/mb_teste/entrypoint.sh"]
