from flask_restplus import Api as _Api
from flask import Flask
from flask import url_for

from mb_teste import VERSION
from .representations import output_json


class ApiComFix(_Api):
    @property
    def specs_url(self):
        """Fixes issue where swagger-ui makes a call to swagger.json over HTTP.
           This can ONLY be used on servers that actually use HTTPS.  On servers that use HTTP,
           this code should not be used at all.
        """
        return url_for(self.endpoint('specs'))


api = ApiComFix(version="version: {}".format(VERSION), title="Serviço mb_teste",
                description="",
                doc='/docs/')
# sobrescreve o encoder original do restpluss
api.representations['application/json'] = output_json
