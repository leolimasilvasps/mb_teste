from flask_restplus import Resource
from werkzeug.exceptions import BadRequest
from mb_teste.api import api
from .schema import RESPONSE_MMS, RANGE
from .service import MMSSrv
from http import HTTPStatus

import logging


log = logging.getLogger(__name__)

ns = api.namespace('teste', description='')
parser = ns.parser()

parser.add_argument('from', type=int, location='args', required=True)
parser.add_argument('to', type=int, location='args')
parser.add_argument('range', type=int, choices=RANGE, location='args', required=True)


@ns.route('/<string:pair>/mms')
class MediaMovelViewer(Resource):
    @ns.doc(parser=parser, validate=True)
    @ns.marshal_with(RESPONSE_MMS, as_list=True, code=HTTPStatus.OK)
    def get(self, pair: str):
        dados_parser = parser.parse_args()
        range = dados_parser['range']
        from_ = dados_parser['from']
        to = dados_parser.get('to')
        media_movel = MMSSrv(pair=pair, range=range, from_=from_, to=to)
        response = media_movel.get()
        return response, HTTPStatus.OK


@ns.route('/check')
class CheckViewer(Resource):
    def get(self):
        return {'status': 'ok'}, HTTPStatus.OK
