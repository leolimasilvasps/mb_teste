from mb_teste.database.models import MediaMovelSimples
from werkzeug.exceptions import BadRequest
from datetime import datetime, timedelta
from mb_teste.database.enum import EnumPair


import logging


log = logging.getLogger(__name__)


class MMSSrv:
    DIAS_LIMITE_CONSULTA = 365

    def __init__(self, pair: str, range: int, from_: int, to: int) -> None:
        self.hoje = datetime.now()
        self.pair = self.valida_pair(pair)
        self.range = range
        self.from_ = self.validar_from(from_)
        self.to = self.validar_to(to)

    def validar_to(self, to):
        if not to:
            return self.hoje - timedelta(days=1)
        return datetime.fromtimestamp(to)

    def validar_from(self, from_: int) -> int:
        if (self.hoje - datetime.fromtimestamp(from_)).days > self.DIAS_LIMITE_CONSULTA:
            log.warning(f"from não pode ser inferior a {self.DIAS_LIMITE_CONSULTA} dias")
            raise BadRequest(f"from não pode ser inferior a {self.DIAS_LIMITE_CONSULTA} dias")
        return datetime.fromtimestamp(from_)

    def valida_pair(self, pair):
        pair = pair.upper()
        if pair not in EnumPair.to_values():
            log.warning(f"pair não encontrado {pair}")
            raise BadRequest(f"pair não encontrado {pair}")
        return pair

    def get(self):
        return MediaMovelSimples.get_by_range_timestamp(self.from_, self.to, self.range)
