from mb_teste.api import api
from flask_restplus import fields
from mb_teste.database.enum import EnumRange

RANGE = EnumRange.to_values()


RESPONSE_MMS = api.model('RESPONSE_MMS', {
    'timestamp': fields.Integer(),
    'mms': fields.Float()
})
