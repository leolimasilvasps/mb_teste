from typing import Dict, List
import requests
import logging
from mb_teste.config import Config

log = logging.getLogger(__name__)


def get_candle_mb(pair: str, from_: int, to: int) -> List[Dict]:
    cfg = Config()
    url = f'{cfg.get_url_mb()}{pair.upper()}/candle'
    data = {
        "from": from_,
        "to": to,
        "precision": '1d'
    }
    response = requests.get(url, data=data)
    if not response.ok:
        log.error(response.text, exc_info=True)
        raise Exception(f'Erro ao consulta api MB {response.text}')
    return response.json()
