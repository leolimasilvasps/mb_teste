from flask import Flask, Blueprint
from flask_cors import CORS
from mb_teste.api import api
import logging
import logging.config
from mb_teste.api.teste.viewer import ns as teste


from mb_teste.api.representations import CustomJSONEncoder
from mb_teste.config import Config, PACKAGE_DIR
from mb_teste.extensions import db, migrate

from apscheduler.schedulers.background import BackgroundScheduler


HORA = 3600


def configure_schedulers():
    # OBS: api do MB nao e publica nao consegui fazer request fora da web
    from mb_teste.script.atualizacao_diaria import att_diaria
    from mb_teste.script.valida_dados import validar_ultimos_360_dias
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(att_diaria, 'interval', days=1)
    sched.add_job(validar_ultimos_360_dias, 'interval', days=1)
    sched.start()


def create_app(config_filename=None):
    shandler = logging.StreamHandler()
    shandler.setLevel(logging.DEBUG)
    configure_schedulers()
    app = Flask(__name__)
    cfg = Config()
    configure_app(app, cfg)
    configure_extensions(app)
    configure_api(app)
    app.config['SWAGGER_BASEPATH'] = '/docs'
    app.config['RESTPLUS_MASK_SWAGGER'] = False
    app.json_encoder = CustomJSONEncoder
    app.logger.addHandler(shandler)
    logging.config.fileConfig(f'{PACKAGE_DIR}/config/logging.conf')
    return app


def configure_app(app, config=None):
    if not config:
        config = Config()
    database_uri = config.getDatabase()['uri']
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # habilitar p/ usar signals/Observer pattern
    app.config['SQLALCHEMY_DATABASE_URI'] = database_uri
    app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {"pool_pre_ping": True, "pool_recycle": 6 * HORA}
    app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024 * 1024


def configure_extensions(app):
    """configure flask extensions
    """
    # para facilitar a configuracao do alembic, importe todos os Base models do SQLAlchemy usados
    # noinspection PyUnresolvedReferences
    from mb_teste.database.models import Model

    db.init_app(app)
    migrate.init_app(app, db)


def configure_api(app):
    blueprint = Blueprint('api', __name__)
    api.init_app(blueprint)
    api.add_namespace(teste, '/teste')
    app.register_blueprint(blueprint)
    CORS(app)
