import enum


def _cria_metodo_is_atributo(atributo, nome_funcao, enumeracao):
    setattr(enumeracao, nome_funcao, lambda x: getattr(x, atributo) is x)


def is_atributo(enumeracao):
    for atributo in enumeracao.to_names():
        nome_funcao = f'is_{atributo.lower()}'
        _cria_metodo_is_atributo(atributo, nome_funcao, enumeracao)
    return enumeracao


@enum.unique
class EnumBase(enum.Enum):
    @classmethod
    def to_values(cls):
        return [e.value for e in cls]

    @classmethod
    def to_names(cls):
        return list(cls.__members__.keys())


@is_atributo
class EnumPair(EnumBase):
    BRLBTC = 'BRLBTC'
    BRLETH = 'BRLETH'


@is_atributo
class EnumRange(EnumBase):
    mms_20 = 20
    mms_50 = 50
    mms_200 = 200
