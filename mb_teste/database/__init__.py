from sqlalchemy import func
from sqlalchemy_utils import generic_repr
from mb_teste.extensions import db
from sqlalchemy.inspection import inspect


def writable_attributes(model):
    """Return column and writable hybrid properties names"""
    from sqlalchemy.ext.hybrid import hybrid_property
    from sqlalchemy.orm.attributes import InstrumentedAttribute
    names = []
    descriptors = dict(inspect(model).all_orm_descriptors)
    for name, item in descriptors.items():
        if isinstance(item, hybrid_property) and item.expr and item.fset:
            names.append(name)
        elif isinstance(item, InstrumentedAttribute):
            names.append(name)
    return names


@generic_repr
class CRUDMixin:
    def __init__(self, *, clean_kwargs=False, **kwargs):

        if clean_kwargs:
            kwargs = self.clean_kwargs(kwargs)
        super(CRUDMixin, self).__init__(**kwargs)

    @classmethod
    def clean_kwargs(cls, kwargs):
        cols = set(writable_attributes(cls))
        return {k: v for k, v in kwargs.items() if k in cols}

    @classmethod
    def create(cls, clean_kwargs=False, **kwargs):
        if clean_kwargs:
            kwargs = cls.clean_kwargs(kwargs)
        instance = cls(**kwargs)
        try:
            return instance.save()
        except Exception as e:
            db.session.rollback()
            raise e

    def save(self, commit=True):
        db.session.add(self)
        if commit:
            db.session.commit()
        return self


class Model(CRUDMixin, db.Model):
    """
    Base model class that includes CRUD convenience methods and a primary key column named id

    # From Mike Bayer's "Building the app" talk
    # https://speakerdeck.com/zzzeek/building-the-app
    """

    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    data_criacao = db.Column(db.DateTime, server_default=func.now())
    data_atualizacao = db.Column(db.DateTime, server_default=func.now(), server_onupdate=func.now())

    @classmethod
    def get_by_id(cls, record_id):
        """Get record by ID."""
        if any(
                (isinstance(record_id, (str, bytes)) and record_id.isdigit(),
                 isinstance(record_id, (int, float))),
        ):
            return cls.query.get(int(record_id))
        return None
