from mb_teste.database import Model
from sqlalchemy.dialects.postgresql import ENUM
from mb_teste.extensions import db
from .enum import EnumPair
from .constante import TAMANHO_PADRAO_DECIMAL_TAXA
from sqlalchemy.ext.hybrid import hybrid_property
from datetime import datetime


class MediaMovelSimples(Model):
    class MMS:
        def __init__(self, mms: float, timestamp: int) -> None:
            self.mms = mms
            self.timestamp = timestamp

    __tablename__ = 'mms'

    __table_args__ = (
        db.UniqueConstraint(
            'pair', 'timestamp',
            name='uq_pair_timestamp'),
    )

    pair = db.Column(ENUM(EnumPair),  nullable=False)
    _timestamp = db.Column('timestamp', db.TIMESTAMP(timezone=False), nullable=False)
    mms_20 = db.Column(db.Numeric(*TAMANHO_PADRAO_DECIMAL_TAXA), nullable=False)
    mms_50 = db.Column(db.Numeric(*TAMANHO_PADRAO_DECIMAL_TAXA), nullable=False)
    mms_200 = db.Column(db.Numeric(*TAMANHO_PADRAO_DECIMAL_TAXA), nullable=False)

    @hybrid_property
    def timestamp(self):
        return datetime.timestamp(self._timestamp)

    @timestamp.setter
    def timestamp(self, value):
        self._timestamp = datetime.fromtimestamp(value)

    @timestamp.expression
    def timestamp(self):
        return self._timestamp

    @classmethod
    def get_by_range_timestamp(cls, from_: int, to: int, range: int):
        q = db.session.query(cls).filter(cls.timestamp >= from_).filter(cls.timestamp <= to).all()
        dados = list(map(lambda d: cls.MMS(getattr(d, f'mms_{range}'), int(d.timestamp)), q))
        return dados


class Log(Model):
    __tablename__ = 'logs'
    logger = db.Column(db.String)
    level = db.Column(db.String)
    trace = db.Column(db.String)
    msg = db.Column(db.String)

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return "<Log: %s - %s>" % (self.created_at.strftime('%m/%d/%Y-%H:%M:%S'), self.msg[:50])
