import pytest
from mb_teste.extensions import db
from alchemy_mock.mocking import UnifiedAlchemyMagicMock
from mb_teste.app import app as api


@pytest.fixture
def client():
    api.url_map.strict_slashes = False
    api.config['TESTING'] = True
    api.config['DEBUG'] = False

    with api.test_client() as client:
        with api.app_context():
            db.session = UnifiedAlchemyMagicMock()
        yield client
