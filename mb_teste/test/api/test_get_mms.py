from mb_teste.database.models import MediaMovelSimples
from unittest.mock import patch
import pytest

from_ = 1630551600
to = 1630551600
range_ = 20
pair = 'BRLBTC'

obj_response_get = [MediaMovelSimples.MMS(mms=2000.00, timestamp=to)]


@patch.object(MediaMovelSimples, 'get_by_range_timestamp', return_value=obj_response_get)
def test_get_mms(mms, client):
    resp = resp = client.get(f'/teste/{pair}/mms?from={from_}&to={to}&range={range_}')
    assert resp.status_code == 200
    assert isinstance(resp.json, list)
    assert len(resp.json) == 1
    assert next(iter(resp.json))['mms'] == 2000.00
    assert next(iter(resp.json))['timestamp'] == from_


@patch.object(MediaMovelSimples, 'get_by_range_timestamp', return_value=obj_response_get)
def test_get_mms_default_to(mms, client):
    resp = client.get(f'/teste/{pair}/mms?from={from_}&range={range_}')
    assert resp.status_code == 200
    assert isinstance(resp.json, list)
    assert len(resp.json) == 1
    assert next(iter(resp.json))['mms'] == 2000.00
    assert next(iter(resp.json))['timestamp'] == from_


def test_get_mms_limit_date(client):
    resp = client.get(f'/teste/{pair}/mms?from=1567306800&range={range_}')
    assert resp.status_code == 400
    assert isinstance(resp.json, dict)
    assert resp.json['message'] == 'from não pode ser inferior a 365 dias'


def test_check(client):
    resp = client.get(f'/teste/check')
    assert resp.status_code == 200
    assert isinstance(resp.json, dict)
