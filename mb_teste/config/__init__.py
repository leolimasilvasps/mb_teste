#!/usr/bin/env python
import os
from mb_teste import PACKAGE_DIR

CONFIG_DIR = os.path.join(PACKAGE_DIR, 'config')


class Config:
    def __init__(self):
        self.env = self._setupEnv()

    def _setupEnv(self):
        env = os.getenv('ENV', 'development')
        env = (env if env.strip() != "" else 'docker')
        return env

    def is_production(self):
        return self.env == 'production'

    def getDatabase(self):
        entry = {}
        entry['uri'] = os.environ['DATA_BASE_URI']
        return entry

    def get_url_mb(self):
        return os.environ['URL_MB']

    def get_carga_inicial_velas(self, pair):
        import json
        with open(f'{CONFIG_DIR}/carga_inicial_{pair.upper()}.json', 'r') as j:
            json_data = json.load(j)
        return json_data
