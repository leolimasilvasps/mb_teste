"""Extensions registry

All extensions here are used as singletons and
initialized in application factory
"""
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from sqlalchemy import MetaData
import simplejson as json
from functools import partial
import re

dumps = partial(json.dumps, default=str, use_decimal=True)



def camel2snake(name):
    first_cap_re = re.compile('(.)([A-Z][a-z]+)')
    all_cap_re = re.compile('([a-z0-9])([A-Z])')
    s1 = first_cap_re.sub(r'\1_\2', name)
    return all_cap_re.sub(r'\1_\2', s1).lower()


def ck_name(constraint, table):
    return str(camel2snake(constraint.__class__.__name__))


naming_convention = {
    "ck_fn": ck_name,
    "ix": "ix_%(column_0_N_label)s",
    "uq": "uq_%(table_name)s_%(column_0_N_name)s",
    "ck": "ck_%(table_name)s_%(column_0_N_name)s_%(ck_fn)s",
    "fk": "fk_%(table_name)s_%(column_0_N_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}

metadata = MetaData(naming_convention=naming_convention)

db = SQLAlchemy(metadata=metadata, engine_options={
    'json_serializer': dumps,  # o serializer json precisa informado no momento da criacao da engine
    # ref (https://docs.sqlalchemy.org/en/13/dialects/postgresql.html#sqlalchemy.dialects.postgresql.JSON
})
migrate = Migrate()
