import os


def get_version():
    return '0.0.1'


PACKAGE_DIR = os.path.dirname(os.path.realpath(__file__))
VERSION = get_version()
