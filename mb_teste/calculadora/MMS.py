from typing import Dict, List
import pandas as pd


class MMSCalc:
    def __init__(self, dados_velas: List[Dict]) -> None:
        self.df = pd.DataFrame(dados_velas)

    def calcular(self) -> List[Dict]:
        self.df['mms_20'] = self.df['close'].rolling(window=20).mean()
        self.df['mms_50'] = self.df['close'].rolling(window=50).mean()
        self.df['mms_200'] = self.df['close'].rolling(window=200).mean()
        return self.df.to_dict('records')
