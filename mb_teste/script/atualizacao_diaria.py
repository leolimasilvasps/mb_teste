from datetime import datetime, timedelta
from mb_teste.calculadora.MMS import MMSCalc
from mb_teste.utils import get_candle_mb
from mb_teste.database.models import MediaMovelSimples, db, EnumPair
from math import isnan

import logging


log = logging.getLogger(__name__)


def tente_insert(mms_calculados, par):
    for d in mms_calculados:
        try:
            if isnan(d['mms_200']):
                continue
            m = MediaMovelSimples(pair=par, **d, clean_kwargs=True)
            m.save()
        except Exception as e:
            log.error("erro na rotina de inserir MMS {e}", exc_info=True)
            db.session.rollback()


def calcular(de, ate, pair):
    dados_mb = get_candle_mb(pair, de, ate)
    c = MMSCalc(dados_mb['candles'])
    mms_calculados = c.calcular()
    tente_insert(mms_calculados, pair)


def att_diaria():
    print('Job de atualizacao de MMS')
    hoje = datetime.now()
    data_ant = hoje - timedelta(days=220)

    for pair in EnumPair.to_values():
        calcular(data_ant, hoje, pair)
