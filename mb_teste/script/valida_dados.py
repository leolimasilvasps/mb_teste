from mb_teste.database.models import MediaMovelSimples
from datetime import datetime, timedelta
import logging


log = logging.getLogger(__name__)


TOTAL_DADOS = 360


def validar(qtd_cadastrado):
    if TOTAL_DADOS is not qtd_cadastrado:
        log.warning("Faltando dados na base dos ultimos 360 dias")
        # Fixme: envia um alerta no slack


def validar_ultimos_360_dias():
    print('Job validar se os ultimos 360 estao cadastrado')
    hoje = datetime.now()
    data_ant = hoje - timedelta(days=360)
    qtd_cadastrado = len(MediaMovelSimples.get_by_range_timestamp(data_ant, hoje))
    validar(qtd_cadastrado)
