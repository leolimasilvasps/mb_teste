# coding: utf-8
from setuptools import setup, find_packages

# install requirements of the project
with open("requirements.txt") as req:
    install_requires = req.read()

setup(name='mb_teste',
      version="0.0.1",
      description="",
      url="",
      author="",
      author_email="",
      license="BSD",
      keywords="mb_teste",
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires
      )
